$(document).ready(function () {

    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 240
        edge: 'right', // Choose the horizontal origin
        closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
    });
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 1, // Creates a dropdown of 15 years to control year
        monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
        monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jui', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        today: 'auj.',
        clear: 'effacer',
        close: 'fermer',
        formatSubmit: 'yyyy/mm/dd'
    });
    $('.tooltipped').tooltip({delay: 50});
    $('.modal-trigger').leanModal();
    $('.slider').slider({
        full_width: true,
        indicators: false
    });

    $('#downloadphoto_zi').click(function () {
        $('#photozoneinsalubreinput').trigger('click');
    });

    $('#photozoneinsalubreinput').change(function () {
        //console.log(this.value);
        var file = document.querySelector('#photozoneinsalubreinput').files[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            var imgPreview = document.getElementById('preview_photo_zi');
            imgPreview.src = reader.result;
            $("#imgicon").hide();
            $('#preview_photo_zi_container').fadeIn(2000);
        };

        if (file) {
            reader.readAsDataURL(file); //reads the data as a URL
        }
    });

    $(".zoneinsalubremodaldetail").click(function () {
        var idarray = (this.id).split('-');
        var id = idarray[1];
        localStorage.setItem('ev_current_zi', id);
        setTimeout(function () {
            detailZoneInsalubre(id);
        }, 1500);

    });

    $(".ev-comment-zi-btn").click(function () {
        if (($("#ev_comment_zi").val()).trim() !== '') {
            var id = localStorage.getItem('ev_current_zi');
            var comment = $("#ev_comment_zi").val();
            commenterZoneInsalubre(id, comment);
        }
    });
});

function detailZoneInsalubre(id) {
    $.ajax({
        url: '/zoneinsalubre',
        data: {id: id},
        type: 'GET',
        success: function (response) {
            $("#ev-preloader").hide();
            $("#zoneInsalubremodalContent").html(response);
        },
        error: function () {
        }
    });
}

function test() {
    alert('ok');
}

function commenterZoneInsalubre(id, comment) {
    $.ajax({
        url: '/zoneinsalubre/comment',
        data: {id: id, comment: comment},
        type: 'GET',
        success: function (response) {
            $("#ev_comment_zi").val('');
            $("#ev-preloader").hide();
            $("#zoneInsalubremodalContent").html(response);
        },
        error: function () {
        }
    });
}
