<?php

namespace EV\ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\AdminBundle\Entity\Evenement;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventsController extends Controller {

    public function getAllEventsAction() {
        // Récupération du repository
        $repository = $this->getDoctrine()
                ->getRepository('EVAdminBundle:Evenement');

        // On récupère la liste de tous les évènements
        $events = $repository->findAll();
        $result = array();
        foreach ($events as $e) {
            $result[] = $e->toJSON(true);
        }
        //die(var_dump($result));
        // Préparation du json
        $response = new JsonResponse();
        $response->setData($result);
        return $response;
    }

    public function getTodayEventsAction() {
        // Récupération du repository
        $repository = $this->getDoctrine()
                ->getRepository('EVAdminBundle:Evenement');
        
        // On récupère la liste des évènements en cours aujourd'hui
        $events = $repository->getTodayEvents();
        
        $result = array();
        
        foreach ($events as $e){
            $result[] = $e->toJSON(true);
        }
        
        $response = new JsonResponse();
        $response->setData($result);
        return $response;
    }

}
