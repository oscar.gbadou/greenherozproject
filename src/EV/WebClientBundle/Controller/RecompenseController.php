<?php

namespace EV\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\UserBundle\Entity\Particulier;

class RecompenseController extends Controller {

  public function listAction() {
    $em = $this->getDoctrine()->getManager();
    $recompenses = $em->getRepository('EVAdminBundle:BonPartenaire')->findAll();
    return $this->render('EVWebClientBundle:Recompense:list.html.twig', array(
      'recompenses'=>$recompenses
    ));
  }
}
