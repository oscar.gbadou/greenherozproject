<?php

namespace EV\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\AdminBundle\Entity\CommentaireEvenement;
use EV\AdminBundle\Entity\Evenement;
use EV\AdminBundle\Entity\ParticiperaiEvenement;
use EV\UserBundle\Entity\Particulier;
use EV\AdminBundle\Form\EvenementType;

class EvenementController extends Controller {

    public function commenterAction($id) {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->get("security.context")->getToken()->getUser();
        if ($currentUser instanceof Particulier) {
            
        } else {
            $currentUser = $em->getRepository('EVUserBundle:Particulier')->findOneByUsername($currentUser);
        }
        $request = $this->get('request');
        $commentaire = $request->get('commentaire');
        $evenement = $em->getRepository('EVAdminBundle:Evenement')->find($id);
        $newCommentaire = new CommentaireEvenement();
        $newCommentaire->setContenu($commentaire)
                ->setEvenement($evenement)
                ->setParticulier($currentUser);
        $em->persist($newCommentaire);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Votre commentaire a été enrégistré avec succès');
        return $this->redirect($this->generateUrl('ev_web_client_participant_evenement', array(
                            'idZoneInsalubre' => $evenement->getZoneInsalubre()->getId()
        )));
    }

    public function detailsEvenementAction($idZoneInsalubre) {
        $em = $this->getDoctrine()->getManager();
        $evenementZoneInsalubre = $em->getRepository('EVAdminBundle:Evenement')->findOneByZoneInsalubre($idZoneInsalubre);
        $participants = $em->getRepository("EVAdminBundle:ParticiperaiEvenement")->findByEvenement($evenementZoneInsalubre);
        $commentaires = $em->getRepository('EVAdminBundle:CommentaireEvenement')->findBy(array('evenement' => $evenementZoneInsalubre), array('date' => 'DESC'));
        return $this->render('EVWebClientBundle:Evenement:detail.html.twig', array(
                    'participants' => $participants,
                    'evenement' => $evenementZoneInsalubre,
                    'commentaires' => $commentaires
        ));
    }

    public function rendrePropreAction($idZoneInsalubre) {
        $currentUser = $this->get("security.context")->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        if ($currentUser instanceof Particulier) {
            
        } else {
            $currentUser = $em->getRepository('EVUserBundle:Particulier')->findOneByUsername($currentUser);
        }
        $zoneInsalubre = $em->getRepository('EVAdminBundle:ZoneInsalubre')->find($idZoneInsalubre);
        $evenementZoneInsalubre = $em->getRepository('EVAdminBundle:Evenement')->findOneByZoneInsalubre($zoneInsalubre);
//        die(var_dump($evenementZoneInsalubre));
        if ($evenementZoneInsalubre) {
            $oldParticiperai = $em->getRepository('EVAdminBundle:ParticiperaiEvenement')->findOneBy(array(
                'particulier' => $currentUser,
                'evenement' => $evenementZoneInsalubre
            ));
            if ($oldParticiperai) {
                $this->get('session')->getFlashBag()->add('success', 'Vous participez déjà à cet événement');
                return $this->redirect($this->generateUrl('ev_web_client_participant_evenement', array('idZoneInsalubre' => $idZoneInsalubre)));
            } else {
                $participerai = new ParticiperaiEvenement();
                $participerai->setDate(new \DateTime());
                $participerai->setEvenement($evenementZoneInsalubre);
                $participerai->setParticulier($currentUser);
                $em->persist($participerai);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Votre participation a été enrégistré avec succès');
                return $this->redirect($this->generateUrl('ev_web_client_participant_evenement', array('idZoneInsalubre' => $idZoneInsalubre)));
            }
        } else {
            $evenement = new Evenement();
            $evenement->setDate(new \DateTime());
            $evenement->setHeure(new \DateTime());
            $form = $this->createForm(new EvenementType(), $evenement);
            $request = $this->get('request');
            if ($request->getMethod() == 'POST') {
                $form->bind($request);
                if ($form->isValid()) {
                    $evenement->setParticulier($currentUser);
                    $evenement->setZoneInsalubre($zoneInsalubre);
                    $zoneInsalubre->setCampagneCreer(true);
                    $em->persist($evenement);
                    $em->flush();

                    $participerai = new ParticiperaiEvenement();
                    $participerai->setDate(new \DateTime());
                    $participerai->setEvenement($evenement);
                    $participerai->setParticulier($currentUser);
                    $em->persist($participerai);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'Votre événement a été enrégistré avec succès');
                    return $this->redirect($this->generateUrl('ev_web_client_zone_insalubre'));
                }
            }
            return $this->render('EVWebClientBundle:Evenement:creerEvenement.html.twig', array(
                        'form' => $form->createView(),
                        'zoneInsalubre' => $zoneInsalubre
            ));
        }
    }

    public function listEvenementAction() {
        $em = $this->getDoctrine()->getManager();
        $evenements = $em->getRepository('EVAdminBundle:Evenement')->findAll();
        $evenementsPasser = $em->getRepository('EVAdminBundle:Evenement')
                ->createQueryBuilder('e')
                ->where('e.date < :now')
                ->setParameter('now', new \DateTime())
                ->getQuery()
                ->getResult();
        $evenementsAVenir = $em->getRepository('EVAdminBundle:Evenement')
                ->createQueryBuilder('e')
                ->where('e.date >= :now')
                ->setParameter('now', new \DateTime())
                ->getQuery()
                ->getResult();
        return $this->render('EVWebClientBundle:Evenement:listEvenement.html.twig', array(
                    'evenementsPasser' => $evenementsPasser,
                    'evenementsAVenir' => $evenementsAVenir
        ));
    }

}
