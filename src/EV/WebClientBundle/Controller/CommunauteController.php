<?php

namespace EV\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\UserBundle\Entity\Particulier;
use EV\AdminBundle\Entity\Communaute;
use EV\AdminBundle\Form\CommunauteType;
use Symfony\Component\HttpFoundation\Request;

class CommunauteController extends Controller {

  public function listAction() {
    $em = $this->getDoctrine()->getManager();
    $communautes = $em->getRepository('EVAdminBundle:Communaute')->findBy(array('valider'=>true));

    /*$token_url = 'https://api.twitter.com/oauth2/token';

    $fields = array (
      'registration_ids' => array ($id),
      'data' => array ("message" => $message, "type"=>$typeNotif)
    );
    $fields = json_encode ( $fields );
    $headers = array (
      'Authorization: key=' . "AIzaSyCXU1WJYrRpmOKcXPpaYCy6_RTsei9HSZ8",
      'Content-Type: application/json'
    );
    $ch = curl_init ();
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

    $result = curl_exec ( $ch );
    $json = json_decode($result);*/

    return $this->render('EVWebClientBundle:Communaute:list.html.twig', array(
      'communautes'=>$communautes
    ));
  }

  public function addCommunauteAction(Request $request){
    $currentUser = $this->get("security.context")->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $communaute = new Communaute();
    $form = $this->createForm(new CommunauteType(), $communaute);
    $request = $this->get('request');
    if ($request->getMethod() == 'POST') {
      $form->bind($request);
      if ($form->isValid()) {
        $communaute->setAuteur($currentUser);
        $em->persist($communaute);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'La demande de création de votre communauté a été enrégistrée avec succès');
        return $this->redirect($this->generateUrl('ev_web_client_communaute'));
      }
    }
    return $this->render('EVWebClientBundle:Communaute:add.html.twig', array(
      'form' => $form->createView()
    ));
  }
}
