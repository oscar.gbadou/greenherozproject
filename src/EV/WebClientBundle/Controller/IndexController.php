<?php

namespace EV\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\UserBundle\Entity\Particulier;

class IndexController extends Controller {

    public function indexAction() {
        return $this->render('EVWebClientBundle:Index:index.html.twig');
    }

    public function accueilAction() {
        return $this->render('EVWebClientBundle:Index:accueil.html.twig');
    }

    public function marcherAction() {
        return $this->render('EVWebClientBundle:Index:marcher.html.twig');
    }
}
