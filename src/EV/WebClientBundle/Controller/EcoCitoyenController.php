<?php

namespace EV\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\UserBundle\Entity\Particulier;

class EcoCitoyenController extends Controller {

    public function lesEcoCitoyensAction() {
        $em = $this->getDoctrine()->getManager();
        $ecocitoyens = $em->getRepository('EVUserBundle:Particulier')
                ->createQueryBuilder('p')
                ->orderBy('p.point', 'DESC')
                ->getQuery()
                ->getResult();
        return $this->render('EVWebClientBundle:EcoCitoyen:lesEcoCitoyens.html.twig', array(
                    'ecocitoyens' => $ecocitoyens
        ));
    }
}
