<?php

namespace EV\WebClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EV\AdminBundle\Entity\ZoneInsalubre;
use EV\AdminBundle\Entity\Document;
use EV\AdminBundle\Entity\CommentaireZonneInsalubre;

class ZoneInsalubreController extends Controller {

    public function login2Action() {
        $this->get('session')->getFlashBag()->add('success', 'Vous pouvez signaler une zone insalubre maintenant');
        return $this->redirect($this->generateUrl('ev_web_client_zone_insalubre'));
    }

    public function zoneInsalubreAction() {
        $em = $this->getDoctrine()->getManager();
        $villeActiver = $em->getRepository('EVAdminBundle:Localite')->findBy(
                array(
            'idtypelocalite' => 2,
            'activer' => true
                ), array(
            'libelle' => 'ASC'
                )
        );
        if (count($villeActiver) == 1) {
            $quartiers = $em->getRepository("EVAdminBundle:Localite")
                    ->createQueryBuilder("l")
                    ->where("l.codelocalite LIKE :codeLocalite")
                    ->andWhere("l.idtypelocalite = 4")
                    ->setParameter("codeLocalite", $villeActiver[0]->getCodeLocalite() . '%')
                    ->orderBy("l.libelle", "ASC")
                    ->getQuery()
                    ->getResult();
        }
        $zoneInsalubre = $em->getRepository("EVAdminBundle:ZoneInsalubre")->findAll();
        $zoneInsalubreLePlusCommenter = $em->getRepository('EVAdminBundle:ZoneInsalubre')
                ->findOneBy(array(), array('nbreCommentaire' => 'DESC'));
        return $this->render('EVWebClientBundle:ZoneInsalubre:zoneInsalubre.html.twig', array(
                    'zoneInsalubre' => $zoneInsalubre,
                    'quartiers' => (isset($quartiers)) ? $quartiers : null,
                    'ville' => $villeActiver,
                    'zoneInsalubreLePlusCommenter' => $zoneInsalubreLePlusCommenter
        ));
    }

    public function signalerZoneInsalubreAction(Request $request) {
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $villeId = $request->get('ville');
        $quartierId = $request->get('quartier');
        $lat = $request->get('lat');
        $lon = $request->get('lon');
        $description = $request->get('description');
        $photo = (isset($_FILES['photo'])) ? $_FILES['photo'] : null;
//        var_dump($photo);
        $ville = $em->getRepository('EVAdminBundle:Localite')->find($villeId);
        $quartier = $em->getRepository('EVAdminBundle:Localite')->find($quartierId);

        if ($photo) {
            $tmp = $_FILES['photo']['tmp_name'];
            $err = $_FILES['photo']['error'];
            $defaultName = $_FILES['photo']['name'];
            $extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
            if (!$err) {
                $newDocument = new Document();
                $newDocument->setAlt($defaultName)
                        ->setUrl($extension);
                $em->persist($newDocument);
                $em->flush();
                $nameExplode = explode('.', $defaultName);
                $newName = __DIR__ . '/../../../../web/uploads/document/' . $newDocument->getId() . '.' . $extension;
                $photoUploader = move_uploaded_file($tmp, $newName);
            }
//            die('ok');
        }

        $newZoneInsalubre = new ZoneInsalubre();
        $newZoneInsalubre->setDescription($description)
                ->setLat($lat)
                ->setLon($lon)
                ->setPhoto((isset($newDocument) ? $newDocument : null))
                ->setParticulier($currentUser)
                ->setQuartier($quartier)
                ->setVille($ville);
        $em->persist($newZoneInsalubre);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Votre alerte a été enrégistrer avec succès');
        return $this->redirect($this->generateUrl('ev_web_client_zone_insalubre'));
    }

    public function detailZoneInsalubreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $zoneInsalubre = $em->getRepository('EVAdminBundle:ZoneInsalubre')->find(intval($id));
        $zoneInsalubre->setNbreCommentaire(count($zoneInsalubre->getCommentaires()));
        $em->flush();
        return $this->container->get('templating')->renderResponse('EVWebClientBundle:ZoneInsalubre:detailZoneInsalubreTemplate.html.twig', array(
                    'zoneInsalubre' => $zoneInsalubre
        ));
    }

    public function commenterZoneInsalubreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $id = $request->get('id');
        $zoneInsalubre = $em->getRepository('EVAdminBundle:ZoneInsalubre')->find(intval($id));
        $comment = $request->get('comment');
        $zoneInsalubre->setNbreCommentaire(count($zoneInsalubre->getCommentaires()) + 1);
        $newComment = new CommentaireZonneInsalubre();
        $newComment->setContenu($comment)
                ->setParticulier($currentUser)
                ->setZoneInsalubre($zoneInsalubre);
        $em->persist($newComment);
        $em->flush();
        return $this->container->get('templating')->renderResponse('EVWebClientBundle:ZoneInsalubre:detailZoneInsalubreTemplate.html.twig', array(
                    'zoneInsalubre' => $zoneInsalubre
        ));
    }

    public function zoneInsalubreDeLaMemeZoneAction($id) {
        $em = $this->getDoctrine()->getManager();
        $zoneInsalubre = $em->getRepository('EVAdminBundle:ZoneInsalubre')->find($id);
        $quartier = $em->getRepository('EVAdminBundle:Localite')->find($zoneInsalubre->getQuartier()->getId());
        $zoneInsalubres = $em->getRepository('EVAdminBundle:ZoneInsalubre')->findByQuartier($quartier);
        return $this->render('EVWebClientBundle:ZoneInsalubre:zoneInsalubreDeLaMemeZone.html.twig', array(
                    'zoneInsalubres' => $zoneInsalubres,
                    'zoneInsalubre' => $zoneInsalubre
        ));
    }

}
