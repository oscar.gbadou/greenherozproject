<?php

namespace EV\UserBundle\Listener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class RegistrationConfirmListener implements EventSubscriberInterface {

    private $router;
    private $container;

    public function __construct(UrlGeneratorInterface $router, ContainerInterface $container) {
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents() {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm'
        );
    }

    public function onRegistrationConfirm(\FOS\UserBundle\Event\GetResponseUserEvent $event) {
        $url = $this->router->generate('ev_web_client_zone_insalubre');
        $this->container->get('session')->getFlashBag()->add('success', 'Vous appartenez désormais à la commuauté éco-citoyenne de votre quartier. Consulter ci-dessous les zones insalubres.');
        $event->setResponse(new RedirectResponse($url));
    }

}
