<?php

namespace EV\UserBundle\Controller;

use PUGX\MultiUserBundle\Controller\RegistrationManager as BaseManager;

class RegistrationParticulierManager extends BaseManager {

    public function register($class) {
        $this->userDiscriminator->setClass($class);
        
        $this->controller->setContainer($this->container);
        $result = $this->controller->registerAction($this->container->get('request'));
        if ($result instanceof RedirectResponse) {
            return $result;
        }
        
        $template = $this->userDiscriminator->getTemplate('registration');
        if (is_null($template)) {
            $engine = $this->container->getParameter('fos_user.template.engine');
            $template = 'FOSUserBundle:Registration:register.html.' . $engine;
        }
        
        $form = $this->formFactory->createForm();
        
        return $this->container->get('templating')->renderResponse($template, array(
                    'form' => $form->createView(),
        ));
    }

}
