<?php

namespace EV\UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use EV\UserBundle\EVUserEvents;
use EV\UserBundle\Entity\Particulier;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class FOSRegistrationParticulierController extends BaseController {

    public function registerAction(Request $request) {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        if ($user instanceof Particulier) {
            $user->addRole("ROLE_PARTICULIER");
        }
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }
        $form = $formFactory->createForm();


        $form->setData($user);

        if ('POST' === $request->getMethod()) {
            $form->bind($request);

            if ($form->isValid()) {
//                $event = new FormEvent($form, $request);
//                $dispatcher->dispatch(EVUserEvents::REGISTRATION_SUCCESS, $event);


                $userManager->updateUser($user);
                $this->authenticateUser($user);
                $url = $this->generateUrl('ev_web_client_zone_insalubre');
                $this->container->get('session')->getFlashBag()->add('success', 'Vous appartenez désormais à la commuauté éco-citoyenne de votre quartier. Consulter ci-dessous les zones insalubres.');
//                if (null === $response = $event->getResponse()) {
//                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
//                    $response = new RedirectResponse($url);
//                }
//
//                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, new \FOS\UserBundle\Event\GetResponseUserEvent($user, $request));
//                return $response;
//                die(var_dump($url));
                header('Location: http://localhost/ecoville/web/app_dev.php/zone-insalubre');
//                return $this->redirect($url);
            }
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    private function authenticateUser($user) {
        $providerKey = 'main'; // your firewall name
        $token = new UsernamePasswordToken(
                $user->getUsername(), $user->getPassword(), $providerKey, $user->getRoles()
        );
//        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->container->get('security.context')->setToken($token);
    }

}
