<?php

namespace EV\UserBundle\Controller;

use \FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityParticulierController extends BaseController {

    protected function renderLogin(array $data) {
        $template = sprintf('EVUserBundle:Security:particulier_login.html.twig');

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
