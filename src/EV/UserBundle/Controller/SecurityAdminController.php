<?php

namespace EV\UserBundle\Controller;

use \FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityAdminController extends BaseController {

    protected function renderLogin(array $data) {
        $template = sprintf('EVUserBundle:Security:admin_login.html.twig');

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
