<?php

namespace EV\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\UserBundle\Entity\Particulier;
use EV\UserBundle\Form\ParticulierType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Particulier controller.
 *
 */
class ParticulierController extends Controller {

  public function quartierCotonouAction(){
    $em = $this->getDoctrine()->getManager();
    $quartiers = $em->getRepository("EVAdminBundle:Localite")
    ->createQueryBuilder('l')
    ->where("l.codelocalite LIKE :codeLocalite")
    ->andWhere('l.idtypelocalite = 4')
    ->orderBy('l.libelle', 'ASC')
    ->setParameter("codeLocalite", '0801' . '%')
    ->getQuery()
    ->getResult();
    return $this->render('EVUserBundle:Particulier:quartierCotonou.html.twig', array(
                'quartiers' => $quartiers,
    ));
  }

  public function registerParticulierAction(Request $request){
    $em = $this->getDoctrine()->getManager();
    $oldUser = $em->getRepository("EVUserBundle:Particulier")->findOneByEmail($request->get('email'));
    if(!$oldUser){
      $quartier = $em->getRepository("EVAdminBundle:Localite")->find(intval($request->get('quartier')));
      $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
      $discriminator->setClass('EV\UserBundle\Entity\Particulier', false);
      $userManager = $this->container->get('pugx_user_manager');
      $newUserM = $userManager->createUser();
      $newUserM->setNom($request->get('nom'));
      $newUserM->setPrenom($request->get('prenom'));
      $newUserM->setEmail($request->get('email'));
      $newUserM->setPlainPassword($request->get('password'));
      $newUserM->setTelephone($request->get('telephone'));
      $newUserM->setCommunaute($quartier);
      $newUserM->setEnabled(true);
      $newUserM->addRole('ROLE_PARTICULIER');
      $userManager->updateUser($newUserM, true);
      $this->get('session')->getFlashBag()->add('success', 'Votre compte a été créé avec succès. Connectez-vous!');
      return $this->redirect($this->generateUrl('ev_particulier_login'));
    }else{
      $this->get('session')->getFlashBag()->add('error', 'Cette adresse email a ete deja utilise');
      return $this->redirect($this->generateUrl('ev_particulier_login'));
    }

  }

    /**
     * Lists all Particulier entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EVUserBundle:Particulier')->findAll();

        return $this->render('EVUserBundle:Particulier:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Particulier entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Particulier();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setEnabled(true);
            $entity->addRole("ROLE_PARTICULIER");
            $em->persist($entity);
            $em->flush();

//            $this->authenticateUser($entity);

            $this->get('session')->getFlashBag()->add('success', 'Votre compte a été créé avec succès');
            return $this->redirect($this->generateUrl('ev_particulier_login'));
        }

        return $this->render('EVUserBundle:Particulier:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    private function authenticateUser($user) {
        $providerKey = 'main'; // your firewall name
        $token = new UsernamePasswordToken(
                $user->getUsername(), $user->getPassword(), $providerKey, $user->getRoles()
        );
//        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

        $this->container->get('security.context')->setToken($token);
    }

    /**
     * Creates a form to create a Particulier entity.
     *
     * @param Particulier $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Particulier $entity) {
        $form = $this->createForm(new ParticulierType($entity), $entity, array(
            'action' => $this->generateUrl('particulier_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Particulier entity.
     *
     */
    public function newAction() {
        $entity = new Particulier();
        $form = $this->createCreateForm($entity);

        return $this->render('EVUserBundle:Particulier:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Particulier entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EVUserBundle:Particulier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particulier entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EVUserBundle:Particulier:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Particulier entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EVUserBundle:Particulier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particulier entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EVUserBundle:Particulier:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Particulier entity.
     *
     * @param Particulier $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Particulier $entity) {
        $form = $this->createForm(new ParticulierType(), $entity, array(
            'action' => $this->generateUrl('particulier_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Particulier entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EVUserBundle:Particulier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particulier entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('particulier_edit', array('id' => $id)));
        }

        return $this->render('EVUserBundle:Particulier:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Particulier entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EVUserBundle:Particulier')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Particulier entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('particulier'));
    }

    /**
     * Creates a form to delete a Particulier entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('particulier_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
