<?php
namespace EV\UserBundle;

final class EVUserEvents {
    const REGISTRATION_SUCCESS = 'cielo_user.registration.success';

    const REGISTRATION_COMPLETED = 'cielo_user.registration.completed';
}
