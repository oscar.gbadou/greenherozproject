<?php

namespace EV\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use EV\UserBundle\Entity\Utilisateur;

/**
 * Admin
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\UserBundle\Entity\AdminRepository")
 * @UniqueEntity(fields = "email", targetClass = "EV\UserBundle\Entity\Utilisateur", message="fos_user.email.already_used")
 */
class Admin extends Utilisateur {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

}
