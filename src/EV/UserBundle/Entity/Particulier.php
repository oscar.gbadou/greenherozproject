<?php

namespace EV\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use EV\UserBundle\Entity\Utilisateur;
use EV\AdminBundle\Utils\Utils;

/**
 * Particulier
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\UserBundle\Entity\ParticulierRepository")
 * @UniqueEntity(fields = "email", targetClass = "EV\UserBundle\Entity\Utilisateur", message="fos_user.email.already_used")
 */
class Particulier extends Utilisateur {

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Localite")
     */
    private $communaute;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var integer
     *
     * @ORM\Column(name="point", type="integer", nullable=true)
     */
    private $point;

    /**
     * Set nom
     *
     * @param string $nom
     * @return Particulier
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Particulier
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Particulier
     */
    public function setTelephone($telephone) {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone() {
        return $this->telephone;
    }


    /**
     * Set point
     *
     * @param integer $point
     * @return Particulier
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set communaute
     *
     * @param \EV\AdminBundle\Entity\Localite $communaute
     * @return Particulier
     */
    public function setCommunaute(\EV\AdminBundle\Entity\Localite $communaute = null)
    {
        $this->communaute = $communaute;

        return $this;
    }

    /**
     * Get communaute
     *
     * @return \EV\AdminBundle\Entity\Localite
     */
    public function getCommunaute()
    {
        return $this->communaute;
    }

    public function toJSON($toArray = false) {

      $array = array(
        'id' => $this->id,
        'nom' => $this->nom,
        'prenom' => $this->prenom,
        'telephone' => $this->telephone,
        'point'=>$this->point
      );
      if ($toArray) {
        return $array;
      } else {
        return Utils::jsonRemoveUnicodeSequences(json_encode($array));
      }
    }
}
