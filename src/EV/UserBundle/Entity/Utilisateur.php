<?php

namespace EV\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Utilisateur
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\UserBundle\Entity\UtilisateurRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"particulier" = "Particulier", "admin" = "Admin"})
 */
abstract class Utilisateur extends BaseUser {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    public function __construct() {
        parent::__construct();
    }

    public function setEmail($email) {
        parent::setEmail($email);
        $this->setUsername($email);
        return $this;
    }

}
