<?php

namespace EV\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationParticulierFormType extends BaseType {

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EV\UserBundle\Entity\Particulier'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);
        $builder
                ->add('nom', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Nom',
                        'class' => 'form-control'
                    )
                ))
                ->add('prenom', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Prénom',
                        'class' => 'form-control'
                    )
                ))
                ->add('telephone', 'text', array(
                    'required' => true,
                    'attr' => array(
                        'placeholder' => 'Téléphone',
                        'class' => 'form-control'
                    )
                ))
                ->add('email', 'email', array(
                    'label' => 'form.email',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => 'Email',
                        'class' => 'form-control'
                    )
                ))
                ->remove('username', null, array(
                    'label' => 'form.username',
                    'translation_domain' => 'FOSUserBundle',
                    'attr' => array(
                        'placeholder' => 'Nom d\'utilisateur',
                        'class' => 'form-control'
                    )
                ))
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array(
                        'label' => 'form.password',
                        'attr' => array(
                            'placeholder' => 'Mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'second_options' => array(
                        'label' => 'form.password_confirmation',
                        'attr' => array(
                            'placeholder' => 'Resaisir mot de passe',
                            'class' => 'form-control'
                        )
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->add('communaute', 'entity', array(
                    'class' => "EVAdminBundle:Communaute",
                    'property' => "libelle",
                    'empty_value' => 'Choisissez votre quartier',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
        ;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ev_userbundle_particulier';
    }

}
