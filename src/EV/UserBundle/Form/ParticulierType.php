<?php

namespace EV\UserBundle\Form;

//use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use EV\UserBundle\Form\UtilisateurType;
use Doctrine\ORM\EntityRepository;

class ParticulierType extends UtilisateurType {

  /**
  * @param FormBuilderInterface $builder
  * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options) {
    parent::buildForm($builder, $options);
    $builder
    ->add('nom', 'text', array(
      'attr' => array(
        'placeholder' => 'Nom',
        'class' => 'form-control'
      )
    ))
    ->add('prenom', 'text', array(
      'attr' => array(
        'placeholder' => 'Prénom',
        'class' => 'form-control'
      )
    ))
    ->add('telephone', 'text', array(
      'required' => true,
      'attr' => array(
        'placeholder' => 'Téléphone',
        'class' => 'form-control'
      )
    ))
    ->remove('username', null, array(
      'label' => 'form.username',
      'translation_domain' => 'FOSUserBundle',
      'attr' => array(
        'placeholder' => 'Nom d\'utilisateur',
        'class' => 'form-control'
      )
    ))
    ->add('plainPassword', 'repeated', array(
      'type' => 'password',
      'options' => array('translation_domain' => 'FOSUserBundle'),
      'first_options' => array(
        'label' => 'form.password',
        'attr' => array(
          'placeholder' => 'Mot de passe',
          'class' => 'form-control'
        )
      ),
      'second_options' => array(
        'label' => 'form.password_confirmation',
        'attr' => array(
          'placeholder' => 'Resaisir mot de passe',
          'class' => 'form-control'
        )
      ),
      'invalid_message' => 'fos_user.password.mismatch',
    ))
    ->add('email', 'email', array(
      'label' => 'form.email',
      'translation_domain' => 'FOSUserBundle',
      'attr' => array(
        'placeholder' => 'Email',
        'class' => 'form-control'
      )
    ))
    ->add('communaute', 'entity', array(
      'class' => "EVAdminBundle:Localite",
      'property' => "libelle",
      'empty_value' => 'Choisissez votre quartier',
      'required' => true,
      'attr' => array(
        'class' => 'browser-default'
      ),
      'query_builder' => function(EntityRepository $er) {
        return $er->createQueryBuilder('l')
        ->where("l.codelocalite LIKE :codeLocalite")
        ->andWhere('l.idtypelocalite = 4')
        ->orderBy('l.libelle', 'ASC')
        ->setParameter("codeLocalite", '0801' . '%');
      }
      ))
      ;
    }

    /**
    * @param OptionsResolverInterface $resolver
    */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
      $resolver->setDefaults(array(
        'data_class' => 'EV\UserBundle\Entity\Particulier'
      ));
    }

    /**
    * @return string
    */
    public function getName() {
      return 'ev_userbundle_particulier';
    }

  }
