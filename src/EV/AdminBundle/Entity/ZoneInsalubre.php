<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EV\AdminBundle\Utils\Utils;

/**
 * ZoneInsalubre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\ZoneInsalubreRepository")
 */
class ZoneInsalubre {

    /**
     * @ORM\OneToMany(targetEntity="EV\AdminBundle\Entity\CommentaireZonneInsalubre", mappedBy="zoneInsalubre", cascade={"remove"})
     */
    private $commentaires;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Document")
     */
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Localite")
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Localite")
     */
    private $quartier;

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=255)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lon", type="string", length=255)
     */
    private $lon;

    /**
     * @var string
     *
     * @ORM\Column(name="actif", type="string", length=255)
     */
    private $actif;

    /**
     * @var string
     *
     * @ORM\Column(name="campagneCreer", type="string", length=255, nullable=true)
     */
    private $campagneCreer;

    /**
     * @var int
     *
     * @ORM\Column(name="nbreCommentaire", type="integer", nullable=true)
     */
    private $nbreCommentaire;

    public function __construct() {
        $this->date = new \DateTime();
        $this->actif = ' ';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ZoneInsalubre
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ZoneInsalubre
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return ZoneInsalubre
     */
    public function setLat($lat) {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat() {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return ZoneInsalubre
     */
    public function setLon($lon) {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string
     */
    public function getLon() {
        return $this->lon;
    }

    /**
     * Set photo
     *
     * @param \EV\AdminBundle\Entity\Document $photo
     * @return ZoneInsalubre
     */
    public function setPhoto(\EV\AdminBundle\Entity\Document $photo = null) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \EV\AdminBundle\Entity\Document
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return ZoneInsalubre
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null) {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier
     */
    public function getParticulier() {
        return $this->particulier;
    }

    /**
     * Set actif
     *
     * @param string $actif
     * @return ZoneInsalubre
     */
    public function setActif($actif) {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return string
     */
    public function getActif() {
        return $this->actif;
    }

    /**
     * Set campagneCreer
     *
     * @param string $campagneCreer
     * @return ZoneInsalubre
     */
    public function setCampagneCreer($campagneCreer) {
        $this->campagneCreer = $campagneCreer;

        return $this;
    }

    /**
     * Get campagneCreer
     *
     * @return string
     */
    public function getCampagneCreer() {
        return $this->campagneCreer;
    }

    /**
     * Set ville
     *
     * @param \EV\AdminBundle\Entity\Localite $ville
     * @return ZoneInsalubre
     */
    public function setVille(\EV\AdminBundle\Entity\Localite $ville = null) {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \EV\AdminBundle\Entity\Localite
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Set quartier
     *
     * @param \EV\AdminBundle\Entity\Localite $quartier
     * @return ZoneInsalubre
     */
    public function setQuartier(\EV\AdminBundle\Entity\Localite $quartier = null) {
        $this->quartier = $quartier;

        return $this;
    }

    /**
     * Get quartier
     *
     * @return \EV\AdminBundle\Entity\Localite
     */
    public function getQuartier() {
        return $this->quartier;
    }


    /**
     * Set nbreCommentaire
     *
     * @param integer $nbreCommentaire
     * @return ZoneInsalubre
     */
    public function setNbreCommentaire($nbreCommentaire)
    {
        $this->nbreCommentaire = $nbreCommentaire;

        return $this;
    }

    /**
     * Get nbreCommentaire
     *
     * @return integer
     */
    public function getNbreCommentaire()
    {
        return $this->nbreCommentaire;
    }

    /**
     * Add commentaires
     *
     * @param \EV\AdminBundle\Entity\CommentaireZonneInsalubre $commentaires
     * @return ZoneInsalubre
     */
    public function addCommentaire(\EV\AdminBundle\Entity\CommentaireZonneInsalubre $commentaires)
    {
        $this->commentaires[] = $commentaires;

        return $this;
    }

    /**
     * Remove commentaires
     *
     * @param \EV\AdminBundle\Entity\CommentaireZonneInsalubre $commentaires
     */
    public function removeCommentaire(\EV\AdminBundle\Entity\CommentaireZonneInsalubre $commentaires)
    {
        $this->commentaires->removeElement($commentaires);
    }

    /**
     * Get commentaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    public function toJSON($toArray = false) {
      $now = new \DateTime();
      $commentaires = $this->commentaires;
      $resultCommentaire = array();
      foreach($commentaires as $c){
        $resultCommentaire[] = $c->toJSON(true);
      }

      if($this->photo){
        $photo = 'http://' . $_SERVER['SERVER_NAME'] . '/web/' . $this->photo->getWebPath();
      }else{
        $photo = null;
      }

      $array = array(
        'id' => $this->id,
        'date' => $this->date->format('d-m-Y H:i:s'),
        'description' => $this->description,
        'lat' => $this->lat,
        'lon' => $this->lon,
        'actif' => $this->actif,
        'campagneCreer' => $this->campagneCreer,
        'nbreCommentaire' => $this->nbreCommentaire,
        'auteur' => $this->particulier->toJSON(true),
        'quartier' => $this->quartier->getLibelle(),
        'ville' => $this->ville->getLibelle(),
        'photo' => $photo,
        'commentaires' => $resultCommentaire
      );
      if ($toArray) {
        return $array;
      } else {
        return Utils::jsonRemoveUnicodeSequences(json_encode($array));
      }
    }
}
