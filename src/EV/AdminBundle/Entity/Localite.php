<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localite
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\LocaliteRepository")
 */
class Localite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="activer", type="boolean", nullable=true)
     */
    private $activer;

    /**
     * @var integer
     *
     * @ORM\Column(name="idtypelocalite", type="integer")
     */
    private $idtypelocalite;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="idlocalitemere", type="integer", nullable = true)
     */
    private $idlocalitemere;

    /**
     * @var string
     *
     * @ORM\Column(name="codelocalite", type="string", length=255)
     */
    private $codelocalite;

    /**
     * @var string
     *
     * @ORM\Column(name="libunique", type="string", length=255)
     */
    private $libunique;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idtypelocalite
     *
     * @param integer $idtypelocalite
     * @return Localite
     */
    public function setIdtypelocalite($idtypelocalite)
    {
        $this->idtypelocalite = $idtypelocalite;

        return $this;
    }

    /**
     * Get idtypelocalite
     *
     * @return integer
     */
    public function getIdtypelocalite()
    {
        return $this->idtypelocalite;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Localite
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set idlocalitemere
     *
     * @param integer $idlocalitemere
     * @return Localite
     */
    public function setIdlocalitemere($idlocalitemere)
    {
        $this->idlocalitemere = $idlocalitemere;

        return $this;
    }

    /**
     * Get idlocalitemere
     *
     * @return integer
     */
    public function getIdlocalitemere()
    {
        return $this->idlocalitemere;
    }

    /**
     * Set codelocalite
     *
     * @param string $codelocalite
     * @return Localite
     */
    public function setCodelocalite($codelocalite)
    {
        $this->codelocalite = $codelocalite;

        return $this;
    }

    /**
     * Get codelocalite
     *
     * @return string
     */
    public function getCodelocalite()
    {
        return $this->codelocalite;
    }

    /**
     * Set libunique
     *
     * @param string $libunique
     * @return Localite
     */
    public function setLibunique($libunique)
    {
        $this->libunique = $libunique;

        return $this;
    }

    /**
     * Get libunique
     *
     * @return string
     */
    public function getLibunique()
    {
        return $this->libunique;
    }

    /**
     * Set activer
     *
     * @param boolean $activer
     * @return Localite
     */
    public function setActiver($activer)
    {
        $this->activer = $activer;

        return $this;
    }

    /**
     * Get activer
     *
     * @return boolean
     */
    public function getActiver()
    {
        return $this->activer;
    }

    public function toJSON($toArray = false) {
      $now = new \DateTime();
      $array = array(
        'id' => $this->id,
        'libelle' => $this->libelle,
        'couvert'=>($this->activer)?true:false
      );
      if ($toArray) {
        return $array;
      } else {
        return Utils::jsonRemoveUnicodeSequences(json_encode($array));
      }
    }
}
