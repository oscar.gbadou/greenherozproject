<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommunauteParticulier
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\CommunauteParticulierRepository")
 */
class CommunauteParticulier {

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Communaute")
     */
    private $communaute;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return CommunauteParticulier
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null) {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier 
     */
    public function getParticulier() {
        return $this->particulier;
    }


    /**
     * Set communaute
     *
     * @param \EV\AdminBundle\Entity\Communaute $communaute
     * @return CommunauteParticulier
     */
    public function setCommunaute(\EV\AdminBundle\Entity\Communaute $communaute = null)
    {
        $this->communaute = $communaute;

        return $this;
    }

    /**
     * Get communaute
     *
     * @return \EV\AdminBundle\Entity\Communaute 
     */
    public function getCommunaute()
    {
        return $this->communaute;
    }
}
