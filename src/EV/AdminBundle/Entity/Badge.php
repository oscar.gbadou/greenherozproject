<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Badge
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\BadgeRepository")
 */
class Badge
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Document")
     */
    private $icon;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrePoint", type="integer")
     */
    private $nbrePoint;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbrePoint
     *
     * @param integer $nbrePoint
     * @return Badge
     */
    public function setNbrePoint($nbrePoint)
    {
        $this->nbrePoint = $nbrePoint;

        return $this;
    }

    /**
     * Get nbrePoint
     *
     * @return integer 
     */
    public function getNbrePoint()
    {
        return $this->nbrePoint;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Badge
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Badge
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set icon
     *
     * @param \EV\AdminBundle\Entity\Document $icon
     * @return Badge
     */
    public function setIcon(\EV\AdminBundle\Entity\Document $icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return \EV\AdminBundle\Entity\Document 
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
