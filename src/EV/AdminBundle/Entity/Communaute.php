<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Communaute
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\CommunauteRepository")
 */
class Communaute
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $auteur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="motivation", type="text")
     */
    private $motivation;

    /**
     * @var string
     *
     * @ORM\Column(name="valider", type="string", length=255)
     */
    private $valider;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct(){
      $this->date = new \DateTime();
      $this->valider = false;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Communaute
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Communaute
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set motivation
     *
     * @param string $motivation
     *
     * @return Communaute
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;

        return $this;
    }

    /**
     * Get motivation
     *
     * @return string
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * Set valider
     *
     * @param string $valider
     *
     * @return Communaute
     */
    public function setValider($valider)
    {
        $this->valider = $valider;

        return $this;
    }

    /**
     * Get valider
     *
     * @return string
     */
    public function getValider()
    {
        return $this->valider;
    }

    /**
     * Set auteur
     *
     * @param \EV\UserBundle\Entity\Particulier $auteur
     *
     * @return Communaute
     */
    public function setAuteur(\EV\UserBundle\Entity\Particulier $auteur = null)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return \EV\UserBundle\Entity\Particulier
     */
    public function getAuteur()
    {
        return $this->auteur;
    }
}
