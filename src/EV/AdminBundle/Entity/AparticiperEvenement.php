<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AparticiperEvenement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\AparticiperEvenementRepository")
 */
class AparticiperEvenement {

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Evenement")
     */
    private $evenement;

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @ORM\OneToOne(targetEntity="EV\AdminBundle\Entity\Document")
     */
    private $photo;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=255)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lon", type="string", length=255)
     */
    private $lon;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return AparticiperEvenement
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return AparticiperEvenement
     */
    public function setLat($lat) {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat() {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param string $lon
     * @return AparticiperEvenement
     */
    public function setLon($lon) {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return string 
     */
    public function getLon() {
        return $this->lon;
    }


    /**
     * Set evenement
     *
     * @param \EV\AdminBundle\Entity\Evenement $evenement
     * @return AparticiperEvenement
     */
    public function setEvenement(\EV\AdminBundle\Entity\Evenement $evenement = null)
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \EV\AdminBundle\Entity\Evenement 
     */
    public function getEvenement()
    {
        return $this->evenement;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return AparticiperEvenement
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null)
    {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier 
     */
    public function getParticulier()
    {
        return $this->particulier;
    }

    /**
     * Set photo
     *
     * @param \EV\AdminBundle\Entity\Document $photo
     * @return AparticiperEvenement
     */
    public function setPhoto(\EV\AdminBundle\Entity\Document $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \EV\AdminBundle\Entity\Document 
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
