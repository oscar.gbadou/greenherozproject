<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SendSMS
 *
 * @ORM\Table(name="send_sms")
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\SendSMSRepository")
 */
class SendSMS {

    /**
     * @var integer
     *
     * @ORM\Column(name="sql_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $sql_id;

    /**
     * @var string
     *
     * @ORM\Column(name="momt", type="string", length=255, nullable=true)
     */
    private $momt;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=255, nullable=true)
     */
    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="receiver", type="string", length=255, nullable=true)
     */
    private $receiver;

    /**
     * @var string
     *
     * @ORM\Column(name="udhdata", type="blob", nullable=true)
     */
    private $udhdata;

    /**
     * @var string
     *
     * @ORM\Column(name="msgdata", type="text", nullable=true)
     */
    private $msgdata;

    /**
     * @var integer
     *
     * @ORM\Column(name="time", type="bigint", nullable=true)
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="smsc_id", type="string", length=255, nullable=true)
     */
    private $smscId;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=255, nullable=true)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=255, nullable=true)
     */
    private $account;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=true)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="sms_type", type="bigint", nullable=true)
     */
    private $smsType;

    /**
     * @var integer
     *
     * @ORM\Column(name="mclass", type="bigint", nullable=true)
     */
    private $mclass;

    /**
     * @var integer
     *
     * @ORM\Column(name="mwi", type="bigint", nullable=true)
     */
    private $mwi;

    /**
     * @var integer
     *
     * @ORM\Column(name="coding", type="bigint", nullable=true)
     */
    private $coding;

    /**
     * @var integer
     *
     * @ORM\Column(name="compress", type="bigint", nullable=true)
     */
    private $compress;

    /**
     * @var integer
     *
     * @ORM\Column(name="validity", type="bigint", nullable=true)
     */
    private $validity;

    /**
     * @var integer
     *
     * @ORM\Column(name="deferred", type="bigint", nullable=true)
     */
    private $deferred;

    /**
     * @var integer
     *
     * @ORM\Column(name="dlr_mask", type="bigint", nullable=true)
     */
    private $dlrMask;

    /**
     * @var string
     *
     * @ORM\Column(name="dlr_url", type="string", length=255, nullable=true)
     */
    private $dlrUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="pid", type="bigint", nullable=true)
     */
    private $pid;

    /**
     * @var integer
     *
     * @ORM\Column(name="alt_dcs", type="bigint", nullable=true)
     */
    private $altDcs;

    /**
     * @var integer
     *
     * @ORM\Column(name="rpi", type="bigint", nullable=true)
     */
    private $rpi;

    /**
     * @var string
     *
     * @ORM\Column(name="charset", type="string", length=255, nullable=true)
     */
    private $charset;

    /**
     * @var string
     *
     * @ORM\Column(name="boxc_id", type="string", length=255, nullable=true)
     */
    private $boxcId;

    /**
     * @var string
     *
     * @ORM\Column(name="binfo", type="string", length=255, nullable=true)
     */
    private $binfo;

    /**
     * Get sql_id
     *
     * @return integer 
     */
    public function getSqlId() {
        return $this->sql_id;
    }

    /**
     * Set momt
     *
     * @param string $momt
     * @return SendSMS
     */
    public function setMomt($momt) {
        $this->momt = $momt;

        return $this;
    }

    /**
     * Get momt
     *
     * @return string 
     */
    public function getMomt() {
        return $this->momt;
    }

    /**
     * Set sender
     *
     * @param string $sender
     * @return SendSMS
     */
    public function setSender($sender) {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return string 
     */
    public function getSender() {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param string $receiver
     * @return SendSMS
     */
    public function setReceiver($receiver) {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return string 
     */
    public function getReceiver() {
        return $this->receiver;
    }

    /**
     * Set udhdata
     *
     * @param string $udhdata
     * @return SendSMS
     */
    public function setUdhdata($udhdata) {
        $this->udhdata = $udhdata;

        return $this;
    }

    /**
     * Get udhdata
     *
     * @return string 
     */
    public function getUdhdata() {
        return $this->udhdata;
    }

    /**
     * Set msgdata
     *
     * @param string $msgdata
     * @return SendSMS
     */
    public function setMsgdata($msgdata) {
        $this->msgdata = $msgdata;

        return $this;
    }

    /**
     * Get msgdata
     *
     * @return string 
     */
    public function getMsgdata() {
        return $this->msgdata;
    }

    /**
     * Set time
     *
     * @param integer $time
     * @return SendSMS
     */
    public function setTime($time) {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer 
     */
    public function getTime() {
        return $this->time;
    }

    /**
     * Set service
     *
     * @param string $service
     * @return SendSMS
     */
    public function setService($service) {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string 
     */
    public function getService() {
        return $this->service;
    }

    /**
     * Set account
     *
     * @param string $account
     * @return SendSMS
     */
    public function setAccount($account) {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount() {
        return $this->account;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return SendSMS
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set smsType
     *
     * @param integer $smsType
     * @return SendSMS
     */
    public function setSmsType($smsType) {
        $this->smsType = $smsType;

        return $this;
    }

    /**
     * Get smsType
     *
     * @return integer 
     */
    public function getSmsType() {
        return $this->smsType;
    }

    /**
     * Set mclass
     *
     * @param integer $mclass
     * @return SendSMS
     */
    public function setMclass($mclass) {
        $this->mclass = $mclass;

        return $this;
    }

    /**
     * Get mclass
     *
     * @return integer 
     */
    public function getMclass() {
        return $this->mclass;
    }

    /**
     * Set mwi
     *
     * @param integer $mwi
     * @return SendSMS
     */
    public function setMwi($mwi) {
        $this->mwi = $mwi;

        return $this;
    }

    /**
     * Get mwi
     *
     * @return integer 
     */
    public function getMwi() {
        return $this->mwi;
    }

    /**
     * Set coding
     *
     * @param integer $coding
     * @return SendSMS
     */
    public function setCoding($coding) {
        $this->coding = $coding;

        return $this;
    }

    /**
     * Get coding
     *
     * @return integer 
     */
    public function getCoding() {
        return $this->coding;
    }

    /**
     * Set compress
     *
     * @param integer $compress
     * @return SendSMS
     */
    public function setCompress($compress) {
        $this->compress = $compress;

        return $this;
    }

    /**
     * Get compress
     *
     * @return integer 
     */
    public function getCompress() {
        return $this->compress;
    }

    /**
     * Set validity
     *
     * @param integer $validity
     * @return SendSMS
     */
    public function setValidity($validity) {
        $this->validity = $validity;

        return $this;
    }

    /**
     * Get validity
     *
     * @return integer 
     */
    public function getValidity() {
        return $this->validity;
    }

    /**
     * Set deferred
     *
     * @param integer $deferred
     * @return SendSMS
     */
    public function setDeferred($deferred) {
        $this->deferred = $deferred;

        return $this;
    }

    /**
     * Get deferred
     *
     * @return integer 
     */
    public function getDeferred() {
        return $this->deferred;
    }

    /**
     * Set dlrMask
     *
     * @param integer $dlrMask
     * @return SendSMS
     */
    public function setDlrMask($dlrMask) {
        $this->dlrMask = $dlrMask;

        return $this;
    }

    /**
     * Get dlrMask
     *
     * @return integer 
     */
    public function getDlrMask() {
        return $this->dlrMask;
    }

    /**
     * Set dlrUrl
     *
     * @param string $dlrUrl
     * @return SendSMS
     */
    public function setDlrUrl($dlrUrl) {
        $this->dlrUrl = $dlrUrl;

        return $this;
    }

    /**
     * Get dlrUrl
     *
     * @return string 
     */
    public function getDlrUrl() {
        return $this->dlrUrl;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     * @return SendSMS
     */
    public function setPid($pid) {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer 
     */
    public function getPid() {
        return $this->pid;
    }

    /**
     * Set altDcs
     *
     * @param integer $altDcs
     * @return SendSMS
     */
    public function setAltDcs($altDcs) {
        $this->altDcs = $altDcs;

        return $this;
    }

    /**
     * Get altDcs
     *
     * @return integer 
     */
    public function getAltDcs() {
        return $this->altDcs;
    }

    /**
     * Set rpi
     *
     * @param integer $rpi
     * @return SendSMS
     */
    public function setRpi($rpi) {
        $this->rpi = $rpi;

        return $this;
    }

    /**
     * Get rpi
     *
     * @return integer 
     */
    public function getRpi() {
        return $this->rpi;
    }

    /**
     * Set charset
     *
     * @param string $charset
     * @return SendSMS
     */
    public function setCharset($charset) {
        $this->charset = $charset;

        return $this;
    }

    /**
     * Get charset
     *
     * @return string 
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Set boxcId
     *
     * @param string $boxcId
     * @return SendSMS
     */
    public function setBoxcId($boxcId) {
        $this->boxcId = $boxcId;

        return $this;
    }

    /**
     * Get boxcId
     *
     * @return string 
     */
    public function getBoxcId() {
        return $this->boxcId;
    }

    /**
     * Set binfo
     *
     * @param string $binfo
     * @return SendSMS
     */
    public function setBinfo($binfo) {
        $this->binfo = $binfo;

        return $this;
    }

    /**
     * Get binfo
     *
     * @return string 
     */
    public function getBinfo() {
        return $this->binfo;
    }

    /**
     * Set smscId
     *
     * @param string $smscId
     * @return SendSMS
     */
    public function setSmscId($smscId) {
        $this->smscId = $smscId;

        return $this;
    }

    /**
     * Get smscId
     *
     * @return string 
     */
    public function getSmscId() {
        return $this->smscId;
    }

}
