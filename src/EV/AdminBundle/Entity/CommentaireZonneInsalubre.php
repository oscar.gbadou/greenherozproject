<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EV\AdminBundle\Utils\Utils;

/**
 * CommentaireZonneInsalubre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\CommentaireZonneInsalubreRepository")
 */
class CommentaireZonneInsalubre {

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\ZoneInsalubre", inversedBy="commentaires")
     */
    private $zoneInsalubre;

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=255)
     */
    private $contenu;

    public function __construct() {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return CommentaireZonneInsalubre
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return CommentaireZonneInsalubre
     */
    public function setContenu($contenu) {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu() {
        return $this->contenu;
    }

    /**
     * Set zoneInsalubre
     *
     * @param \EV\AdminBundle\Entity\ZoneInsalubre $zoneInsalubre
     * @return CommentaireZonneInsalubre
     */
    public function setZoneInsalubre(\EV\AdminBundle\Entity\ZoneInsalubre $zoneInsalubre = null) {
        $this->zoneInsalubre = $zoneInsalubre;

        return $this;
    }

    /**
     * Get zoneInsalubre
     *
     * @return \EV\AdminBundle\Entity\ZoneInsalubre
     */
    public function getZoneInsalubre() {
        return $this->zoneInsalubre;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return CommentaireZonneInsalubre
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null) {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier
     */
    public function getParticulier() {
        return $this->particulier;
    }

    public function toJSON($toArray = false) {

      $array = array(
        'id' => $this->id,
        'date' => $this->date->format('d-m-Y H:i:s'),
        'contenu' => $this->contenu,
        'auteur' => $this->particulier->toJSON(true),
      );
      if ($toArray) {
        return $array;
      } else {
        return Utils::jsonRemoveUnicodeSequences(json_encode($array));
      }
    }

}
