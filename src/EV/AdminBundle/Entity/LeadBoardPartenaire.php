<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeadBoardPartenaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\LeadBoardPartenaireRepository")
 */
class LeadBoardPartenaire
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Partenaire")
     */
    private $partenaire;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="point", type="integer")
     */
    private $point;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set point
     *
     * @param integer $point
     * @return LeadBoardPartenaire
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return integer 
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set partenaire
     *
     * @param \EV\AdminBundle\Entity\Partenaire $partenaire
     * @return LeadBoardPartenaire
     */
    public function setPartenaire(\EV\AdminBundle\Entity\Partenaire $partenaire = null)
    {
        $this->partenaire = $partenaire;

        return $this;
    }

    /**
     * Get partenaire
     *
     * @return \EV\AdminBundle\Entity\Partenaire 
     */
    public function getPartenaire()
    {
        return $this->partenaire;
    }
}
