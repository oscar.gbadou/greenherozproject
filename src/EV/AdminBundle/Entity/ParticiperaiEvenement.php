<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParticiperaiEvenement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\ParticiperaiEvenementRepository")
 */
class ParticiperaiEvenement {

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Evenement")
     */
    private $evenement;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    public function __construct() {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ParticiperaiEvenement
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return ParticiperaiEvenement
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null) {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier 
     */
    public function getParticulier() {
        return $this->particulier;
    }

    /**
     * Set evenement
     *
     * @param \EV\AdminBundle\Entity\Evenement $evenement
     * @return ParticiperaiEvenement
     */
    public function setEvenement(\EV\AdminBundle\Entity\Evenement $evenement = null) {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \EV\AdminBundle\Entity\Evenement 
     */
    public function getEvenement() {
        return $this->evenement;
    }

}
