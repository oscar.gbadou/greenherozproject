<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MesAchats
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\MesAchatsRepository")
 */
class MesAchats
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Dechet")
     */
    private $dechet;
    
    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return MesAchats
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dechet
     *
     * @param \EV\AdminBundle\Entity\Dechet $dechet
     * @return MesAchats
     */
    public function setDechet(\EV\AdminBundle\Entity\Dechet $dechet = null)
    {
        $this->dechet = $dechet;

        return $this;
    }

    /**
     * Get dechet
     *
     * @return \EV\AdminBundle\Entity\Dechet 
     */
    public function getDechet()
    {
        return $this->dechet;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return MesAchats
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null)
    {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier 
     */
    public function getParticulier()
    {
        return $this->particulier;
    }
}
