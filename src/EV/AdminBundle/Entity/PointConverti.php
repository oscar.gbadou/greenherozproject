<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointConverti
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\PointConvertiRepository")
 */
class PointConverti
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\BonPartenaire")
     */
    private $bonPartenaire;
    
    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bonPartenaire
     *
     * @param \EV\AdminBundle\Entity\BonPartenaire $bonPartenaire
     * @return PointConverti
     */
    public function setBonPartenaire(\EV\AdminBundle\Entity\BonPartenaire $bonPartenaire = null)
    {
        $this->bonPartenaire = $bonPartenaire;

        return $this;
    }

    /**
     * Get bonPartenaire
     *
     * @return \EV\AdminBundle\Entity\BonPartenaire 
     */
    public function getBonPartenaire()
    {
        return $this->bonPartenaire;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return PointConverti
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null)
    {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier 
     */
    public function getParticulier()
    {
        return $this->particulier;
    }
}
