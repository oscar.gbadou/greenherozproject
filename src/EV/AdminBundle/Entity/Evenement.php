<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EV\AdminBundle\Entity\ZoneInsalubre;
use EV\AdminBundle\Utils\Utils;

/**
 * Evenement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\EvenementRepository")
 */
class Evenement {

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\ZoneInsalubre")
     */
    private $zoneInsalubre;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time")
     */
    private $heure;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="adressePhysique", type="text")
     */
    private $adressePhysique;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Evenement
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return Evenement
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null) {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier
     */
    public function getParticulier() {
        return $this->particulier;
    }

    /**
     * Set zoneInsalubre
     *
     * @param \EV\AdminBundle\Entity\ZoneInsalubre $zoneInsalubre
     * @return Evenement
     */
    public function setZoneInsalubre(\EV\AdminBundle\Entity\ZoneInsalubre $zoneInsalubre = null) {
        $this->zoneInsalubre = $zoneInsalubre;

        return $this;
    }

    /**
     * Get zoneInsalubre
     *
     * @return \EV\AdminBundle\Entity\ZoneInsalubre
     */
    public function getZoneInsalubre() {
        return $this->zoneInsalubre;
    }

    public function toJSON($toArray = false) {
      $now = new \DateTime();
      $array = array(
        'id' => $this->id,
        'date' => $this->date->format('d-m-Y'),
        'heure' => $this->heure->format('H:i:s'),
        'libelle' => $this->libelle,
        'description' => $this->description,
        'adressePhysique' => $this->adressePhysique,
        'zoneInsalubre' => $this->zoneInsalubre->toJSON(true),
        'auteur' => $this->particulier->toJSON(true),
      );
      if ($toArray) {
        return $array;
      } else {
        return Utils::jsonRemoveUnicodeSequences(json_encode($array));
      }
    }

    /*public function toJSON($toArray = false) {
        $description = $this->zoneInsalubre->getDescription();
        $date = $this->getDate();

        $zone = $this->getZoneInsalubre();
        $zoneId = $zone->getId();
        $zonePic = $zone->getPhoto();
        $zonePicId = $zonePic->getId();
        $zonePicUrl = 'http://192.168.1.101/ecoville/web/' . $zonePic->getWebPath();
        $zonePicAlt = $zonePic->getAlt();

        $zoneUser = $zone->getParticulier();
        $zoneUserId = $zoneUser->getId();
        $zoneUserNom = $zoneUser->getNom();
        $zoneUserPrenom = $zoneUser->getPrenom();


        $zoneUserCommunaute = $zoneUser->getCommunaute();
        $zoneUserCommunauteId = $zoneUserCommunaute->getId();
        $zoneUserCommunauteLibelle = $zoneUserCommunaute->getLibelle();

        //$photoId = $zone->get
        $array = Array(
            "event_id" => $this->getId(),
            "event_description" => $description,
            "event_date" => $date,
            "event_zone_id" => $zoneId,
            "event_zone_picture_id" => $zonePicId,
            "event_zone_picture_url" => $zonePicUrl,
            "event_zone_picture_alt" => $zonePicAlt,
            "event_zone_user_id" => $zoneUserId,
            "event_zone_user_nom" => $zoneUserNom,
            "event_zone_user_prenom" => $zoneUserPrenom,
            "event_zone_user_communaute_id" => $zoneUserCommunauteId,
            "event_zone_user_communaute_libelle" => $zoneUserCommunauteLibelle,
        );
        if ($toArray)
            return $array;
        else
            return Utils::jsonRemoveUnicodeSequences(json_encode($array));
    }*/


    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Evenement
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set adressePhysique
     *
     * @param string $adressePhysique
     * @return Evenement
     */
    public function setAdressePhysique($adressePhysique)
    {
        $this->adressePhysique = $adressePhysique;

        return $this;
    }

    /**
     * Get adressePhysique
     *
     * @return string
     */
    public function getAdressePhysique()
    {
        return $this->adressePhysique;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Evenement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set heure
     *
     * @param \DateTime $heure
     * @return Evenement
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure
     *
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }
}
