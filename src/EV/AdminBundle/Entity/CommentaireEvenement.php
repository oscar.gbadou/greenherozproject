<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommentaireEvenement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\CommentaireEvenementRepository")
 */
class CommentaireEvenement
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Evenement")
     */
    private $evenement;

    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    public function __construct() {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return CommentaireEvenement
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return CommentaireEvenement
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set evenement
     *
     * @param \EV\AdminBundle\Entity\Evenement $evenement
     * @return CommentaireEvenement
     */
    public function setEvenement(\EV\AdminBundle\Entity\Evenement $evenement = null)
    {
        $this->evenement = $evenement;

        return $this;
    }

    /**
     * Get evenement
     *
     * @return \EV\AdminBundle\Entity\Evenement
     */
    public function getEvenement()
    {
        return $this->evenement;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return CommentaireEvenement
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null)
    {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier
     */
    public function getParticulier()
    {
        return $this->particulier;
    }
}
