<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationAchat
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\NotificationAchatRepository")
 */
class NotificationAchat
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Dechet")
     */
    private $dechet;
    
    /**
     * @ORM\ManyToOne(targetEntity="EV\UserBundle\Entity\Particulier")
     */
    private $particulier;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmer", type="boolean")
     */
    private $confirmer;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return NotificationAchat
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set confirmer
     *
     * @param boolean $confirmer
     * @return NotificationAchat
     */
    public function setConfirmer($confirmer)
    {
        $this->confirmer = $confirmer;

        return $this;
    }

    /**
     * Get confirmer
     *
     * @return boolean 
     */
    public function getConfirmer()
    {
        return $this->confirmer;
    }

    /**
     * Set dechet
     *
     * @param \EV\AdminBundle\Entity\Dechet $dechet
     * @return NotificationAchat
     */
    public function setDechet(\EV\AdminBundle\Entity\Dechet $dechet = null)
    {
        $this->dechet = $dechet;

        return $this;
    }

    /**
     * Get dechet
     *
     * @return \EV\AdminBundle\Entity\Dechet 
     */
    public function getDechet()
    {
        return $this->dechet;
    }

    /**
     * Set particulier
     *
     * @param \EV\UserBundle\Entity\Particulier $particulier
     * @return NotificationAchat
     */
    public function setParticulier(\EV\UserBundle\Entity\Particulier $particulier = null)
    {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \EV\UserBundle\Entity\Particulier 
     */
    public function getParticulier()
    {
        return $this->particulier;
    }
}
