<?php

namespace EV\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BonPartenaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EV\AdminBundle\Entity\BonPartenaireRepository")
 */
class BonPartenaire
{
    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Partenaire")
     */
    private $partenaire;

    /**
     * @ORM\ManyToOne(targetEntity="EV\AdminBundle\Entity\Document", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbrePoint", type="integer")
     */
    private $nbrePoint;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExpiration", type="datetime", nullable=true)
     */
    private $dateExpiration;

    public function __construct(){
      $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return BonPartenaire
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BonPartenaire
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set partenaire
     *
     * @param \EV\AdminBundle\Entity\Partenaire $partenaire
     * @return BonPartenaire
     */
    public function setPartenaire(\EV\AdminBundle\Entity\Partenaire $partenaire = null)
    {
        $this->partenaire = $partenaire;

        return $this;
    }

    /**
     * Get partenaire
     *
     * @return \EV\AdminBundle\Entity\Partenaire
     */
    public function getPartenaire()
    {
        return $this->partenaire;
    }

    /**
     * Set nbrePoint
     *
     * @param integer $nbrePoint
     *
     * @return BonPartenaire
     */
    public function setNbrePoint($nbrePoint)
    {
        $this->nbrePoint = $nbrePoint;

        return $this;
    }

    /**
     * Get nbrePoint
     *
     * @return integer
     */
    public function getNbrePoint()
    {
        return $this->nbrePoint;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return BonPartenaire
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return BonPartenaire
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set dateExpiration
     *
     * @param \DateTime $dateExpiration
     *
     * @return BonPartenaire
     */
    public function setDateExpiration($dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * Get dateExpiration
     *
     * @return \DateTime
     */
    public function getDateExpiration()
    {
        return $this->dateExpiration;
    }

    /**
     * Set image
     *
     * @param \EV\AdminBundle\Entity\Document $image
     *
     * @return BonPartenaire
     */
    public function setImage(\EV\AdminBundle\Entity\Document $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \EV\AdminBundle\Entity\Document
     */
    public function getImage()
    {
        return $this->image;
    }
}
