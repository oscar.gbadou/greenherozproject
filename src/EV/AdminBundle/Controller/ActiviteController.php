<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ActiviteController extends Controller
{
    public function listAction()
    {
      $em = $this->getDoctrine()->getManager();
      $activites = $em->getRepository('EVAdminBundle:Evenement')->findAll();
        return $this->render('EVAdminBundle:Activite:list.html.twig', array(
          'activites'=>$activites
        ));
    }
}
