<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AlerteController extends Controller
{
    public function listAction()
    {
      $em = $this->getDoctrine()->getManager();
      $alertes = $em->getRepository('EVAdminBundle:ZoneInsalubre')->findAll();
        return $this->render('EVAdminBundle:Alerte:list.html.twig', array(
          'alertes'=>$alertes
        ));
    }
}
