<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\AdminBundle\Entity\Partenaire;
use EV\AdminBundle\Form\PartenaireType;
use Symfony\Component\HttpFoundation\Request;

class PartenaireController extends Controller
{
  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $partenaires = $em->getRepository('EVAdminBundle:Partenaire')->findAll();
    return $this->render('EVAdminBundle:Partenaire:list.html.twig', array(
      'partenaires'=>$partenaires
    ));
  }

  public function addAction(Request $request){
    $currentUser = $this->get("security.context")->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $partenaire = new Partenaire();
    $form = $this->createForm(new PartenaireType(), $partenaire);
    $request = $this->get('request');
    if ($request->getMethod() == 'POST') {
      $form->bind($request);
      if ($form->isValid()) {
        $em->persist($partenaire);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Partenaire ajoute avec succès');
        return $this->redirect($this->generateUrl('ev_admin_partenaire_list'));
      }
    }
    return $this->render('EVAdminBundle:Partenaire:add.html.twig', array(
      'form' => $form->createView()
    ));
  }

}
