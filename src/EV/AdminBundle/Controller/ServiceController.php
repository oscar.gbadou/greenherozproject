<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use EV\AdminBundle\Utils\Utils;
use EV\AdminBundle\Entity\ZoneInsalubre;
use EV\AdminBundle\Entity\Document;
use EV\AdminBundle\Entity\ParticiperaiEvenement;
use EV\AdminBundle\Entity\Evenement;

class ServiceController extends Controller
{
  public function testAction()
  {
    $response = new Response();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $response->setContent("OK");
    return $response;
  }

  public function addUserAction(Request $request){

    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    if($request->getMethod() == "POST"){
      $em = $this->getDoctrine()->getManager();
      $type = $request->get('type');
      $nom = $request->get('nom');
      $prenom = $request->get('prenom');
      $telephone = $request->get('telephone');
      $email = $request->get('email');
      $password = $request->get('password');
      $communaute_id = $request->get('communaute');

      if($nom && $prenom && $telephone && $email && $password && $communaute_id){

        $oldUser = $em->getRepository('EVUserBundle:Utilisateur')->findOneByEmail($email);
        $communaute = $em->getRepository('EVAdminBundle:Localite')->find($communaute_id);

        if(!$oldUser){
          $discriminator = $this->container->get('pugx_user.manager.user_discriminator');
          $discriminator->setClass('EV\UserBundle\Entity\Particulier', true);
          $userManager = $this->container->get('pugx_user_manager');
          $newUserM = $userManager->createUser();
          $newUserM->setNom($nom);
          $newUserM->setPrenom($prenom);
          $newUserM->setUsername($email);
          $newUserM->setEmail($email);
          $newUserM->setPlainPassword($password);
          $newUserM->setTelephone($telephone);
          $newUserM->setCommunaute($communaute);
          $newUserM->setEnabled(true);
          $newUserM->addRole('ROLE_PARTICULIER');
          $userManager->updateUser($newUserM, true);

          $response->setData([
            'success' => true,
            'message' => 'Utilisateur enregistre'
          ]);
        }else{
          $response->setData([
            'success' => false,
            'message' => 'Utilisateur existant'
          ]);
        }

      }else{
        $response->setData([
          'success' => false,
          'message' => 'Donnees manquantes'
        ]);
      }
    }else{
      $response->setData([
        'success' => false,
        'message' => 'Mauvaise methode d\'envoi des donnees'
      ]);
    }

    return $response;
  }

  public function loginAction(Request $request){
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    if($request->getMethod() == "POST"){
      $em = $this->getDoctrine()->getManager();
      $email = $request->get('email');
      $password = $request->get('password');

      if($email && $password){
        $discriminator = $this->get('pugx_user.manager.user_discriminator');
        $discriminator->setClass('EV\UserBundle\Entity\Particulier', true);
        $user = $em->getRepository('EVUserBundle:Utilisateur')->findOneByUsername($email);

        if ($user != null) {
          $factory = $this->get('security.encoder_factory');
          $encoder = $factory->getEncoder($user);
          if ($user->getPassword() === $encoder->encodePassword($password, $user->getSalt())) {
            $em->flush();
            $response->setData([
              'success' => true,
              'message' => 'Utilisateur connecte'
            ]);
          } else {
            $response->setData([
              'success' => false,
              'message' => 'Mot de passe ou email incorrect'
            ]);
          }
        } else {
          $response->setData([
            'success' => false,
            'message' => 'Utilisateur inexistant'
          ]);
        }
      }else{
        $response->setData([
          'success' => false,
          'message' => 'Donnees manquantes'
        ]);
      }

    }else{
      $response->setData([
        'success' => false,
        'message' => 'Mauvaise methode d\'envoi des donnees'
      ]);
    }

    return $response;
  }

  public function alertesAction(Request $request){
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $em = $this->getDoctrine()->getManager();
    $alertes = $em->getRepository('EVAdminBundle:ZoneInsalubre')->findBy(array(), array('id'=>'DESC'));
    $result = array();
    foreach($alertes as $a){
      $result[] = $a->toJSON(true);
    }
    $response->setData([
      'success' => true,
      'message' => $result
    ]);
    return $response;
  }

  public function eventsAction(Request $request){
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $em = $this->getDoctrine()->getManager();
    $events = $em->getRepository('EVAdminBundle:Evenement')->findBy(array(), array('id'=>'DESC'));
    $result = array();
    foreach($events as $e){
      $result[] = $e->toJSON(true);
    }
    $response->setData([
      'success' => true,
      'message' => $result
    ]);
    return $response;
  }

  public function createAlerteAction(Request $request){
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    if($request->getMethod() == "POST"){
      $em = $this->getDoctrine()->getManager();
      $villeId = $request->get('ville');
      $quartierId = $request->get('quartier');
      $lat = $request->get('lat');
      $lon = $request->get('lon');
      $description = $request->get('description');
      $email = $request->get('email');
      $photo = (isset($_FILES['photo'])) ? $_FILES['photo'] : null;

      if($villeId && $quartierId && $lat && $lon && $description && $email && $photo){
        $user = $em->getRepository('EVUserBundle:Utilisateur')->findOneByEmail($email);

        if($user){

          $ville = $em->getRepository('EVAdminBundle:Localite')->find($villeId);
          $quartier = $em->getRepository('EVAdminBundle:Localite')->find($quartierId);

          if ($photo) {
            $tmp = $_FILES['photo']['tmp_name'];
            $err = $_FILES['photo']['error'];
            $defaultName = $_FILES['photo']['name'];
            $extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
            if (!$err) {
              $newDocument = new Document();
              $newDocument->setAlt($defaultName)
              ->setUrl($extension);
              $em->persist($newDocument);
              $em->flush();
              $nameExplode = explode('.', $defaultName);
              $newName = __DIR__ . '/../../../../web/uploads/document/' . $newDocument->getId() . '.' . $extension;
              $photoUploader = move_uploaded_file($tmp, $newName);
            }
          }

          $newZoneInsalubre = new ZoneInsalubre();
          $newZoneInsalubre->setDescription($description)
          ->setLat($lat)
          ->setLon($lon)
          ->setPhoto((isset($newDocument) ? $newDocument : null))
          ->setParticulier($user)
          ->setQuartier($quartier)
          ->setVille($ville);
          $em->persist($newZoneInsalubre);
          $em->flush();

          $response->setData([
            'success' => true,
            'message' => 'Alerte enregistree'
          ]);
        }else{
          $response->setData([
            'success' => false,
            'message' => 'Utilisateur inexistant'
          ]);
        }
      }else{
        $response->setData([
          'success' => false,
          'message' => 'Donnees manquantes'
        ]);
      }

    }else{
      $response->setData([
        'success' => false,
        'message' => 'Mauvaise methode d\'envoi des donnees'
      ]);
    }

    return $response;
  }

  public function createEventAction(Request $request){
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    if($request->getMethod() == "POST"){
      $em = $this->getDoctrine()->getManager();
      $email = $request->get('email');
      $date = $request->get('date');
      $heure = $request->get('heure');
      $libelle = $request->get('libelle');
      $adressePhysique = $request->get('adressePhysique');
      $description = $request->get('description');

      if($email && $date && $heure && $libelle && $adressePhysique && $description){
        $currentUser = $em->getRepository('EVUserBundle:Utilisateur')->findOneByEmail($email);
        $zoneInsalubreId = $request->get('zoneInsalubre');
        $zoneInsalubre = $em->getRepository('EVAdminBundle:ZoneInsalubre')->find($zoneInsalubreId);
        $evenementZoneInsalubre = $em->getRepository('EVAdminBundle:Evenement')->findOneByZoneInsalubre($zoneInsalubre);

        if($currentUser){
          if ($evenementZoneInsalubre) {
            $oldParticiperai = $em->getRepository('EVAdminBundle:ParticiperaiEvenement')->findOneBy(array(
              'particulier' => $currentUser,
              'evenement' => $evenementZoneInsalubre
            ));
            if (!$oldParticiperai) {
              $participerai = new ParticiperaiEvenement();
              $participerai->setDate(new \DateTime());
              $participerai->setEvenement($evenementZoneInsalubre);
              $participerai->setParticulier($currentUser);
              $em->persist($participerai);
              $em->flush();
            }
          } else {
            $evenement = new Evenement();
            $evenement->setDate(date_create_from_format('Y-m-d',$date));
            $evenement->setHeure(date_create_from_format('H:i:s',$heure));
            $evenement->setParticulier($currentUser);
            $evenement->setZoneInsalubre($zoneInsalubre)
            ->setLibelle($libelle)
            ->setAdressePhysique($adressePhysique)
            ->setDescription($description);
            $zoneInsalubre->setCampagneCreer(true);
            $em->persist($evenement);
            $em->flush();

            $participerai = new ParticiperaiEvenement();
            $participerai->setDate(new \DateTime());
            $participerai->setEvenement($evenement);
            $participerai->setParticulier($currentUser);
            $em->persist($participerai);
            $em->flush();
          }
          $response->setData([
            'success' => true,
            'message' => 'Evenement enregistre'
          ]);
        }else{
          $response->setData([
            'success' => false,
            'message' => 'Utilisateur inexistant'
          ]);
        }
      }else{
        $response->setData([
          'success' => false,
          'message' => 'Donnees manquantes'
        ]);
      }
    }else{
      $response->setData([
        'success' => false,
        'message' => 'Mauvaise methode d\'envoi des donnees'
      ]);
    }

    return $response;
  }

  public function villesAction(Request $request)
  {
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $em = $this->getDoctrine()->getManager();

    if($request->get('type') != 'all'){
      $villes = $em->getRepository('EVAdminBundle:Localite')
      ->findBy(array('idtypelocalite' => 2, 'activer' => true), array('libelle' => 'ASC'));
    }else{
      $villes = $em->getRepository('EVAdminBundle:Localite')
      ->findBy(array('idtypelocalite' => 2), array('libelle' => 'ASC'));
    }
    $result = array();
    foreach($villes as $v){
      $result[] = $v->toJSON(true);
    }
    $response->setData([
      'success' => true,
      'message' => $result
    ]);
    return $response;
  }

  public function quartiersAction(Request $request)
  {
    $response = new JsonResponse();
    $response->headers->add(array('Access-Control-Allow-Origin' => '*'));
    $ville_id = $request->get('ville_id');
    if($ville_id){
      $em = $this->getDoctrine()->getManager();

      $ville = $em->getRepository("EVAdminBundle:Localite")->find($ville_id);
      $quartiers = $em->getRepository("EVAdminBundle:Localite")
      ->createQueryBuilder("l")
      ->where("l.codelocalite LIKE :codeLocalite")
      ->andWhere("l.idtypelocalite = 4")
      ->setParameter("codeLocalite", $ville->getCodeLocalite() . '%')
      ->orderBy("l.libelle", "ASC")
      ->getQuery()
      ->getResult();
      $result = array();
      foreach ($quartiers as $q) {
        $result[] = array(
          'id' => $q->getId(),
          'libelle' => $q->getLibelle()
        );
      }
      $response->setData([
        'success' => true,
        'message' => $result
      ]);
    }else{
      $response->setData([
        'success' => false,
        'message' => 'Donnees manquantes'
      ]);
    }

    return $response;
  }
}
