<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommunauteController extends Controller
{
  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $communautes = $em->getRepository('EVAdminBundle:Communaute')->findAll();
    return $this->render('EVAdminBundle:Communaute:list.html.twig', array(
      'communautes'=>$communautes
    ));
  }
  public function activerAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $communaute = $em->getRepository('EVAdminBundle:Communaute')->find($id);
    if($communaute && $communaute->getValider()){
      $communaute->setValider(false);
      $em->flush();
    }elseif ($communaute && !$communaute->getValider()) {
      $communaute->setValider(true);
      $em->flush();
    }
    return $this->redirect($this->generateUrl('ev_admin_communaute_list'));
  }
}
