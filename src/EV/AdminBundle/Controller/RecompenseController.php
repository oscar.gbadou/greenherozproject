<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EV\AdminBundle\Entity\BonPartenaire;
use EV\AdminBundle\Form\BonPartenaireType;
use Symfony\Component\HttpFoundation\Request;

class RecompenseController extends Controller
{
  public function listAction()
  {
    $em = $this->getDoctrine()->getManager();
    $recompenses = $em->getRepository('EVAdminBundle:BonPartenaire')->findAll();
    return $this->render('EVAdminBundle:Recompense:list.html.twig', array(
      'recompenses'=>$recompenses
    ));
  }

  public function addAction(Request $request){
    $currentUser = $this->get("security.context")->getToken()->getUser();
    $em = $this->getDoctrine()->getManager();
    $recompense = new BonPartenaire();
    $form = $this->createForm(new BonPartenaireType(), $recompense);
    $request = $this->get('request');
    if ($request->getMethod() == 'POST') {
      $form->bind($request);
      if ($form->isValid()) {
        $recompense->setCode(strtoupper(substr($recompense->getPartenaire()->getNom(), 0, 2)).rand(1000, 9999) . '');
        $em->persist($recompense);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Recompense ajoutee avec succès');
        return $this->redirect($this->generateUrl('ev_admin_recompense_list'));
      }
    }
    return $this->render('EVAdminBundle:Recompense:add.html.twig', array(
      'form' => $form->createView()
    ));
  }

}
