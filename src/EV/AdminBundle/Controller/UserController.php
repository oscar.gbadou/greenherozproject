<?php

namespace EV\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function listAction()
    {
      $em = $this->getDoctrine()->getManager();
      $users = $em->getRepository('EVUserBundle:Particulier')->findAll();
        return $this->render('EVAdminBundle:User:list.html.twig', array(
          'users'=>$users
        ));
    }
}
