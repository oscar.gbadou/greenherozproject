<?php

namespace EV\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EvenementType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('libelle', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Ajouter un nom court et clair'
                    )
                ))
                ->add('adressePhysique', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Donnez plus d\'indication sur l\'endroit'
                    )
                ))
                ->add('description', 'textarea', array(
                    'attr' => array(
                        'class' => 'ev-add-comment-textarea',
                        'placeholder' => 'Décrivez l\'événement',
                        'style' => 'border: 1px solid #777;'
                    )
                ))
                ->add('date', 'date', array(
                    'required' => true,
                    'attr' => array(
                        'type'=>'date'
                    )
                ))
                ->add('heure')
//            ->add('particulier')
//            ->add('zoneInsalubre')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EV\AdminBundle\Entity\Evenement'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ev_adminbundle_evenement';
    }

}
