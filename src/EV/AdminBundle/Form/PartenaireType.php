<?php

namespace EV\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartenaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('secteurActivite', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('adresse', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('telephone', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('email', 'email', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('siteWeb', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('facebook', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('twitter', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('description', 'textarea', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('logo', new DocumentType(), array(
            'label' => 'Logo',
            'attr' => array(
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EV\AdminBundle\Entity\Partenaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ev_adminbundle_partenaire';
    }


}
