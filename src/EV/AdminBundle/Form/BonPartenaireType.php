<?php

namespace EV\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BonPartenaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        /*->add('code', 'text', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))*/
        ->add('partenaire', 'entity', array(
            'placeholder' => 'Choisir le donateur',
            'required' => true,
            'label' => ' ',
            'class' => "EVAdminBundle:Partenaire",
            'choice_label' => "nom",
            'attr' => array(
                'class' => 'form-control border-input'
            )
        ))
        ->add('description', 'textarea', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('nbrePoint', 'integer', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        ->add('quantite', 'integer', array(
          'attr'=>array(
            'class'=>'form-control border-input'
          )
        ))
        /*->add('dateExpiration', 'date', array(
            'attr' => array(
                'class'=>'form-control border-input'
            )
        ))*/
        ->add('image', new DocumentType(), array(
            'label' => 'Image',
            'attr' => array(
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EV\AdminBundle\Entity\BonPartenaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ev_adminbundle_bonpartenaire';
    }


}
