<?php

namespace EV\AdminBundle\Utils;

class Utils {
    static function jsonRemoveUnicodeSequences($struct) {
      if(substr(phpversion(), 0,1) != 7){
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", $struct);
      }else{
        return $struct;
      }
    }
}
